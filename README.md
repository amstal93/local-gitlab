# local-gitlab
Run gitlab locally, with it's own private runner and a private docker registry to store generated images.

## Setup
1. Add the following line to your `/etc/hosts` file:

   ```
   # You can choose any ip address, but I'd recommend a loopback
   # address, or a local bridge or virtual network interface,
   # to prevent other users from being able to connect to it,
   # unless that's what you want.
   127.0.2.2	gitlab-web gitlab-runner gitlab-registry
   # Since all services are on different ports you can overload
   # the same ip address for all of them, but you can also use
   # different loopback addresses for them if you prefer.
   ```

   1. If you pick a different ip address, you'll need to modify the `docker-compose.yml` file to reflect that address in all the `ports:` sections of the file.

2. Start up the stack:

   ```shell
   $ docker-compose up -d ; docker-compose logs -f
   ```

3. Watch the logs go by; when it looks like all the services are browse the this location:
   `http://gitlab-web:8080/`

4. Gitlab will display the initial login page, here you can set up an admin user (you can call it whatever you want, most people use `admin` or `root`), or you can set up your own account here; first enter the password of the administrative login, then proceed to the login page and enter its name and the password you just typed.

5. Now to set up the runner:

   1. Click on the little wrench at the top of the window (Admin Area)

   2. On the left side menu click on "Runners"

   3. On the right side you will see a box with a numbered list, and one of the options will have something like "*Use the following registration token*", copy that token to your clipboard

   4. Run the following command line:

      ```shell
      $ ./register-runner.sh "${PASTE_TOKEN_HERE}"
      ```

      It will "exec" into the gitlab-runner container and pass the token to the runner process, which  will allow it to communicate with the main gitlab process.

   5. After that, you can refresh the gitlab admin page and it should show that you now have an active "Shared" runner, with a description "docker-stable", version number, etc.

## Testing the registry

You can test the registry with a simple job like this one:

1. Create a test project in gitlab-web

2. Add a trivial dockerfile like this one:

   ```dockerfile
   FROM alpine:latest
   RUN date > /timestamp.txt
   ```

3. Create a simple gitlab pipeline to build that image and save it to the registry:

   ```yaml
   build-image:
     # Official docker image.
     image: docker:latest
     stage: build
     before_script:
       - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
     script:
       - docker build --pull -t "$CI_REGISTRY_IMAGE" .
       - docker push "$CI_REGISTRY_IMAGE"
   ```

4. When you run this and it finishes successfully, browse to the "Packages" section of the repository and click on "Container Registry", you should see there the image that was created in this run.

   1. Even though the registry is a separate process from Gitlab, it's still managed by Gitlab, and you can delete images from your registry that you no longer need, click on the little red trash can that's on the right hand side of the specific image tags to delete them from the registry.

Enjoy!!