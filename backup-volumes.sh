#!/bin/ash
# Keep a rotating set of 5 backup files of all volumes.
#
# Environment variables:
#  * BACKUPS_DIR   :- directory where backups are stored [/backups]
#  * BACKUPS_PREFIX :- base name for backup files [vols-]
#  * BACKUPS_PERIOD :- hours between backup runs [4h]
#  * BACKUPS_NUM   :- number of backups to keep [5]
#  * RESTORE_FROM  :- first run will restore from this volume
#
SCRIPT="$(readlink -f "$0")"
OWNERSHIP=$(stat -c %u:%g "$SCRIPT")
DIR=${BACKUPS_DIR:-/backups}
PREFIX=${BACKUPS_PREFIX:-vols-}
NUM=${BACKUPS_NUM:-5}
PERIOD=${BACKUPS_PERIOD:-4h}
RESTORE=${RESTORE_FROM:-no}

################################################################
# Nothing to change beyond this line.
#
main() {
  restore_volumes
  while true; do
    backup_volumes && 
      delete_old_backups
    pause_period
  done
}

restore_volumes() {
  [[ ${RESTORE} == no ]] && return
  local rc=1 bfn=${DIR}/${RESTORE}
  if [[ -f ${bfn} ]]; then
    log info "Restoring volume content from: ${bfn}"
    tar xzvf ${bfn} -C /mnt ; rc=$?
    if [[ $rc -eq 0 ]]; then
      log info "Successfully restored volumes from: $bfn"
      pause_period
    else
      log warn "Caught an error while restoring: rc=$rc"
    fi
  else
    log error "Restore volume not found: $bfn"
  fi
  return $rc
}

backup_volumes() {
  local fn="${DIR}/${PREFIX}$(date +%F%T)"
  log info "Backing up volumes to: $fn.tgz"
  tar czvf $fn.tgz -C /mnt . > $fn.log 2> $fn.err
  local rc=$?
  if [[ $rc -eq 0 ]]; then
    log info "Finished backup [$fn]"
  else
    log error "Caught error while backing up: rc=$rc"
  fi
  chown $OWNERSHIP $fn.*
}

delete_old_backups() {
  local n=$(old_backups | wc -l) pfx
  if [[ $n -gt 0 ]]; then
    log info "Deleting old backup files:"
    for pfx in $(old_backups) ; do
      rm -fv ${pfx}*
      log info "  * $pfx"
    done
  fi
}

old_backups() {
  ls ${DIR}/${PREFIX}* \
    | cut -d. -f1 \
    | sort -u \
    | head -n -$NUM
}

pause_period() {
  log info "Pausing for $PERIOD"
  sleep ${PERIOD}
}

log() {
  echo "[$1] ${@:2}" >&2
}

main

# The End
